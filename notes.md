# Poker Fundamentals

## Standard 52-Card Deck
There are 52 cards. Each card has a rank and suit - there are 13 ranks and 4 suits:
- Suits:
	- Heart
	- Spade
	- Club
	- Diamond
- Ranks
	- Ace
	- Numbers 2 to 10 (inclusive)
	- Jack
	- Queen
	- King

## Texas hold'em
Card rank (highest first) in Texas hold'em: `A K Q J 10 9 8 7 6 5 4 3 2 (A in a straight)`

## Hand Values (Increasing Order)
- Highcard:        Simple value of the card. Lowest: `2`, Highest: `Ace`
- Pair:            Two cards with the same value. E.g `KS KH 7D 2C 5S` or `7D 7C 7D 2C 5S`
- Two pairs:       Two times two cards with the same value. E.g `KS KH 7D 7C 5S`
- Three of a kind: Three cards with the same value. E.g `KC KH KD 5D 6C` - 13 kinds, 4 suits - 13x4 x 3 x 2
- Straight:        Sequence of 5 cards in increasing value. E.g `3C 4H 5D 6C 7S` or `TD JH QC KC AS` or `AS 2D 3D 4C 5H`
- Flush:           5 cards of the same suit. E.g `KC QC 9C 8C 2C`
- Full house:      Combination of three of a kind and a pair: `KC KH KD 7D 7C`
- Four of a kind:  Four cards of the same value. E.g `6S 6D 6H 6C KS`
- Straight flush:  Straight of the same suit. E.g `3S 4S 5S 6S 7S` or `TD JD QD KD AD` or `AC 2C 3C 4C 5C`
- Royal flush:     Straight flush from Ten to Ace. `TS JS QS KS AS` or `TC JC QC KC AC` or `TH JH QH KH AH` or `TD JD QD KD AD`

## Implement hand description
- [x] Highcard
- [x] Pair
- [x] Two pairs
- [x] Three of a kind
- [x] Straight
- [x] Flush
- [x] Full house
- [x] Four of a kind
- [x] Straight flush
- [x] Royal flush
