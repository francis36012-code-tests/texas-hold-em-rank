const readline = require("readline");
const process = require("process");

function getRankValue(rank) {
	switch (rank) {
		case "A":
			return 1;
		case "T":
			return 10;
		case "J":
			return 11;
		case "Q":
			return 12;
		case "K":
			return 13;
		default:
			return Number.parseInt(rank);
	}
}

function getMultiCardRankValue(cards) {
	return cards.reduce((sum, card) => sum + getRankValue(card.rank), 0);
}

function getRankDescription(rank) {
	switch (rank) {
		case "A":
			return "Ace";
		case "T":
			return "10";
		case "J":
			return "Jack";
		case "Q":
			return "Queen";
		case "K":
			return "King";
		default:
			return rank;
	}
}

function getRankFromValue(value) {
	switch (value) {
		case 1:
		case 14:
			return "A";
		case 10:
			return "T";
		case 11:
			return "J";
		case 12:
			return "Q";
		case 13:
			return "K";
		default:
			if (2 <= value && value <= 9) {
				return value + "";
			}
			throw new Error(`invalid rank value: ${value}`);
	}
}

const Suits = new Set(["C", "D", "S", "H"]);
const Ranks = new Set(["A", "2", "3", "4", "5", "6", "7", "8", "9", "T", "J", "Q", "K"]);
function parseCard(input) {
	let trimmedInput = input.trim().toUpperCase();
	if (trimmedInput.length > 2) {
		throw new Error(`invalid card spec: ${trimmedInput}`);
	}
	let rank = input[0];
	let suit = input[1];
	if (!Ranks.has(rank)) {
		throw new Error(`invalid card rank: ${rank}`);
	}
	if (!Suits.has(suit)) {
		throw new Error(`invalid card suit: ${suit}`);
	}
	return {
		rank: rank,
		suit: suit,
	};
}

// Pick k items from n items
function getCombinations(availableItems, count) {
	if (count >= availableItems.length) {
		return [];
	}

	let result = [];

	// This is the easiest approach
	for (let i = 0n; i < 1n << BigInt(availableItems.length); i++) {
		let c = 0n;

		// Count how many bits are set
		for (let j = 0n; j < BigInt(availableItems.length); j++) {
			if (i & (1n << j)) {
				c++;
			}
		}

		if (c == count) {
			let items = [];
			for (let j = 0n; j < BigInt(availableItems.length); j++) {
				if (i & (1n << j)) {
					items.push(availableItems[j]);
				}
			}
			result.push(items);
		}
	}

	return result;
}

function isPair(c1, c2) {
	return c1.rank === c2.rank;
}

function isSameKind(cards) {
	if (cards.length < 2) {
		return true;
	}
	let firstCard = cards[0];
	return cards.every((card) => card.rank === firstCard.rank);
}

function isSameSuit(cards) {
	if (cards.length < 2) {
		return true;
	}
	let firstCard = cards[0];
	return cards.every((card) => card.suit === firstCard.suit);
}

function isStraight(cards) {
	if (cards.length != 5) {
		return { valid: false };
	}

	let cardRankValues = cards.map((card) => getRankValue(card.rank));
	cardRankValues.sort();

	if (cardRankValues[4] == 13 && cardRankValues[0] === 1) {
		cardRankValues.shift();
		cardRankValues.push(14);
	}

	let result = cardRankValues.reduce(
		({ stillStraight, lastRankValue }, rankValue) => {
			return {
				stillStraight: lastRankValue != null ? stillStraight && lastRankValue + 1 === rankValue : true,
				lastRankValue: rankValue,
			};
		},
		{ stillStraight: false, lastRankValue: null }
	);

	return result.stillStraight
		? { valid: true, rank: getRankFromValue(result.lastRankValue) }
		: { valid: false };
}

function isFullHouse(cards) {
	if (cards.length !== 5) {
		return false;
	}
	let cardIndices = [0, 1, 2, 3, 4];
	let tripleCombinations = getCombinations(cardIndices, 3);
	for (let combination of tripleCombinations) {
		let triple = combination.map((idx) => cards[idx]);
		let additionalPair = cardIndices.filter((idx) => !combination.includes(idx)).map((idx) => cards[idx]);
		if (isSameKind(triple) && isSameKind(additionalPair)) {
			return true;
		}
	}
	return false;
}

function getPlayableHands(hands) {
	let result = [];

	for (let hand of hands) {
		// Highcard
		if (hand.length === 1) {
			let card = hand[0];
			let rankValue = card.rank === "A" ? 14 : getRankValue(card.rank);
			result.push([
				hand,
				{
					rank: 1,
					description: `Highcard ${getRankDescription(card.rank)}`,
					value: rankValue,
				},
			]);
			continue;
		}

		// Pair
		if (hand.length === 2) {
			if (isPair(hand[0], hand[1])) {
				result.push([
					hand,
					{
						rank: 2,
						description: `Pair ${getRankDescription(hand[0].rank)}`,
						value: getRankValue(hand[0].rank) * 2,
					},
				]);
			}
			continue;
		}

		if (hand.length === 4) {
			// four of a kind
			if (isSameKind(hand)) {
				result.push([
					hand,
					{
						rank: 8,
						description: `Four of a Kind ${getRankDescription(hand[0].rank)}`,
						value: getRankValue(hand[0].rank) * 4,
					},
				]);
				continue;
			} else {
				// Two pair
				let pair1Card = null;
				let pair2Card = null;

				if (isPair(hand[0], hand[1]) && isPair(hand[2], hand[3])) {
					pair1Card = hand[0];
					pair2Card = hand[2];
				}

				if (isPair(hand[0], hand[2]) && isPair(hand[1], hand[3])) {
					pair1Card = hand[0];
					pair2Card = hand[1];
				}

				if (isPair(hand[0], hand[3]) && isPair(hand[1], hand[2])) {
					pair1Card = hand[0];
					pair2Card = hand[1];
				}

				if (!!pair1Card && !!pair2Card) {
					let pair1Rank = getRankValue(pair1Card.rank);
					let pair2Rank = getRankValue(pair2Card.rank);

					let description = pair1Rank > pair2Rank
						? `Two Pair ${getRankDescription(pair2Card.rank)} ${getRankDescription(pair1Card.rank)}`
						: `Two Pair ${getRankDescription(pair1Card.rank)} ${getRankDescription(pair2Card.rank)}`;

					result.push([
						hand,
						{
							rank: 3,
							description: description,
							value: pair1Rank * 2 + pair2Rank * 2,
						},
					]);
				}
			}
		}

		// Three of a kind
		if (hand.length === 3) {
			if (isSameKind(hand)) {
				result.push([
					hand,
					{
						rank: 4,
						description: `Three of a Kind ${getRankDescription(hand[0].rank)}`,
						value: getRankValue(hand[0].rank) * 3,
					},
				]);
			}
		}

		if (hand.length === 5) {
			let straight = isStraight(hand);

			if (straight.valid && isSameSuit(hand)) {
				if (straight.rank === "A") {
					// Royal Flush
					result.push([
						hand,
						{
							rank: 10,
							description: `Royal Flush`,
							value: getMultiCardRankValue(hand),
						},
					]);
				} else {
					// Straight Flush
					result.push([
						hand,
						{
							rank: 9,
							description: `Straight Flush ${getRankDescription(straight.rank)}`,
							value: getMultiCardRankValue(hand),
						},
					]);
				}
			} else if (isSameSuit(hand)) {
				// Flush
				result.push([
					hand,
					{
						rank: 6,
						description: `Flush ${getRankDescription(hand[4].rank)}`,
						value: getMultiCardRankValue(hand),
					},
				]);
			} else if (straight.valid) {
				// Straight
				result.push([
					hand,
					{
						rank: 5,
						description: `Straight ${getRankDescription(straight.rank)}`,
						value: getMultiCardRankValue(hand),
					},
				]);
			} else if (isFullHouse(hand)) {
				// Straight
				result.push([
					hand,
					{
						rank: 7,
						description: `Full house ${getRankDescription(hand[4].rank)}`,
						value: getMultiCardRankValue(hand),
					},
				]);
			}
		}
	}

	return result;
}

function rank(inputData) {
	let players = inputData.players;
	let communityCards = inputData.communityCards;

	let final = [];

	for (let player of players) {
		let holes = player.holes;

		let combinations = [];
		for (let i = 1; i <= 5; i++) {
			combinations = combinations.concat(getCombinations([...communityCards, ...holes], i));
		}

		// Get the hand with the most value
		let playables = getPlayableHands(combinations);
		playables.sort((h1, h2) => {
			let valueOrd = h1[1].value - h2[1].value;

			return valueOrd === 0
				? h1[1].rank - h2[1].rank
				: valueOrd;
		});

		let highest = playables.slice(-1)[0];
		final.push({
			name: player.name,
			hand: highest[0],
			handValue: highest[1],
			holes: holes,
		});
	}

	// order the players based on hand values
	final.sort((p1, p2) => {
		let rankOrd = p1.handValue.rank - p2.handValue.rank;

		if (rankOrd < 0) {
			return -1;
		} else if (rankOrd > 0) {
			return 1;
		}

		// Same rank value (e.g, both have pairs) so compare total value of hand (sum of rank for cards in hand)
		let valueOrd = p1.handValue.value - p2.handValue.value;
		if (valueOrd < 0) {
			return -1;
		} else if (valueOrd > 0) {
			return 1;
		}

		// Kickers
		if (p1.hand.length === 5) {
			return -1;
		} else if (p2.hand.length === 5) {
			return 1;
		}

		return 0;
	});

	for (let i = final.length - 1; i >= 0; i--) {
		let player = final[i];
		let playerRank = final.length - i;
		console.log(`${playerRank} ${player.name} ${player.handValue.description}`);
	}
}

function readInput() {
	let result = {
		communityCards: [],
		players: [],
	};

	let rl = readline.createInterface({
		input: process.stdin,
	});

	return new Promise((resolve, reject) => {
		let communityCardsRead = false;
		rl.on("line", (input) => {
			if (!communityCardsRead) {
				let cards = input.split(" ").map((card) => parseCard(card));
				result.communityCards = cards;

				communityCardsRead = true;
			} else {
				let playerData = input.split(" ");
				result.players.push({
					name: playerData[0],
					holes: playerData.slice(1).map((card) => parseCard(card)),
				});
			}
		}).on("close", () => {
			if (result.communityCards.length !== 5) {
				reject(new Error("invalid input: 5 community cards must be provided"));
				return;
			}

			if (result.players.length < 2) {
				reject(new Error("invalid input: at least two players required"));
				return;
			}

			resolve(result);
		})
	});
}

readInput()
	.then((input) => {
		rank(input);
	})
	.catch((error) => {
		console.log(`error: ${error?.message ?? message}`);
		console.error(error);
	});
